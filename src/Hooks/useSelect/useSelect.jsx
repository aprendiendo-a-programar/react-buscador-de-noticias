import { useState } from 'react';

const useSelect = (initialState, options) => {

    // State del custom hook
    const [ state, setState] = useState(initialState);

    const SelecNews = () => {
        return (
            <select
                className="browser-default"
                value={state}
                onChange={e => setState(e.target.value)}
            >
                {options.map(option => {
                    return(
                        <option key={option.value} value={option.value}>{option.label}</option>
                    )
                })}
            </select>
        );
    }

    return [state, SelecNews]
}

export default useSelect;