import { useState, useEffect } from 'react';
import { Header, Form, NewsList } from './Components/index';
import './App.css';

function App() {
  // Definir la categoría y noticias
  const [category, saveCategory] = useState('');
  const [news, saveNews] = useState([]);

  useEffect(() => {
    const consultAPI = async () => {
        const url = `https://newsapi.org/v2/top-headlines?country=us&category=${category}&apiKey=6b775a25fdac488d9a4bbdeb283445c3`;

        const response = await fetch(url);
        const news = await response.json();

        saveNews(news.articles);
    }
    consultAPI();
  }, [category]);

  return (
    <>
      <Header
        title="Buscador de noticias"
      />

      <div className="container white">
        <Form
          saveCategory={saveCategory}
        />

        <NewsList
          news={news}
        />
      </div>
    </>
  );
}

export default App;
