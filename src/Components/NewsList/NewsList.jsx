import News from '../News/News';

const NewsList = ({news}) => {
    return (
        <div className="row">

            {news.map((news) => {
                return(
                    <News
                        key={news.url}
                        news={news}
                    />
                )
            })}

        </div>
    );
}
 
export default NewsList;