import Header from './Header/Header';
import Form from './Form/Form';
import NewsList from './NewsList/NewsList';

export {
    Header,
    Form,
    NewsList
}