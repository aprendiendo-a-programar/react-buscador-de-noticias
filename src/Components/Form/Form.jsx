
import useSelect from '../../Hooks/useSelect/useSelect';
import './Form.css';

const Form = ({ saveCategory }) => {

    const OPTIONS = [
        { value: 'general', label: 'General' },
        { value: 'business', label: 'Negocios' },
        { value: 'entertainment', label: 'Entretenimiento' },
        { value: 'health', label: 'Health' },
        { value: 'science', label: 'Science' },
        { value: 'sports', label: 'Sports' },
        { value: 'tecnhology', label: 'Tecnología' }
    ]

    // Utilizar custom hook
    const [category, SelectNews] = useSelect('general', OPTIONS);

    // Submit al form, pasar categoría a app.jsx
    const searchCategory = e => {
        e.preventDefault();

        saveCategory(category);
    }

    return (
        <div className="search row">
            <div className="col s12 m8 offset-m2">
                <form className="search__form" onSubmit={searchCategory}>
                    <h2 className="search__form-title">Encuentra noticias por categorías</h2>

                    <SelectNews />

                    <div className="search__cont-input input-field col s12">
                        <input
                            type="submit"
                            className="search__input btn-large amber darken-2"
                            value="Buscar"
                        />
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Form;